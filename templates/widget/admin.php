<p>
    <label for="<?= esc_attr($this->get_field_id('title')) ?>">
        <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
    </label>
    <input type="text"
           class="widefat"
           id="<?= esc_attr($this->get_field_id('title')) ?>"
           name="<?= esc_attr($this->get_field_name('title')) ?>"
           value="<?= esc_attr($title) ?>"
    />

    <br/>

    <label for="<?= esc_attr($this->get_field_id('base_currency')) ?>">
        <?php esc_attr_e( 'Default base currency:', 'text_domain' ); ?>
    </label>
    <input type="text"
           class="widefat"
           id="<?= esc_attr($this->get_field_id('base_currency')) ?>"
           name="<?= esc_attr($this->get_field_name('base_currency')) ?>"
           value="<?= esc_attr($base_currency) ?>"
    />

    <br/>

    <label for="<?= esc_attr($this->get_field_id('convert_to_currency')) ?>">
        <?php esc_attr_e( 'Default convert to currency:', 'text_domain' ); ?>
    </label>
    <input type="text"
           class="widefat"
           id="<?= esc_attr($this->get_field_id('convert_to_currency')) ?>"
           name="<?= esc_attr($this->get_field_name('convert_to_currency')) ?>"
           value="<?= esc_attr($convert_to_currency) ?>"
    />

    <label for="<?= esc_attr($this->get_field_id('based_on_ip')) ?>">
        <?php esc_attr_e( 'Default Convert to Currency -> based on ip and its country currency:', 'text_domain' ); ?>
    </label>
    <input type="checkbox"
           class="checkbox"
           <?= $based_on_ip ?>
           id="<?= esc_attr($this->get_field_id('based_on_ip')) ?>"
           name="<?= esc_attr($this->get_field_name('based_on_ip')) ?>"
    />
</p>