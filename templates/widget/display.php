<div class="floating-badge calc-wrapper" data-toggle="tooltip" title="<?= $title ?>" data-placement="left">
    <img class="calculator-badge-trigger" src="<?= Crypto_Calculator_URL . 'assets/images/calculator.new.png' ?>" alt="">
    <div class="badge-calculator">
        <div class="header">
            <span class="title">
                <img src="<?= Crypto_Calculator_URL . 'assets/images/calculator.new.png' ?>" alt="">
                <?= $title ?>
            </span>
            <button>
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
        <div class="badge-wrapper">
            <div class="item-wrap">
                <label for="amount" class="label-name"><?= ctranslate('translation_enter_amount') ?></label>
                <input type="number"
                       name="amount"
                       value="1"
                       class="form-control amount calculate-price"
                />
            </div>
            <div class="item-wrap">
                <span class="label-name"><?= ctranslate('translation_base_currency') ?></span>
                <select class="base_list calculate-price form-control"
                        name="base-currency"
                        data-default="<?= $base_currency ?>"
                >
                    <optgroup label="Fiat Currencies">
                        <?php foreach ($currencies as $currency) { ?>
                            <option data-slug="<?= $currency->name ?>"
                                    data-full-name="<?= $currency->full_name ?>"
                                    data-icon="<?= Crypto_Calculator_URL . 'assets/images/currencies/' . strtolower($currency->name) . '.svg' ?>"
                                    value="<?= $currency->price ?>"><?= $currency->full_name ?> (<?= $currency->name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                    <optgroup label="Crypto Currencies">
                        <?php foreach ($crypto_currencies as $crypto_currency) { ?>
                            <option data-slug="<?= $crypto_currency->short_name ?>"
                                    data-full-name="<?= $crypto_currency->name ?>"
                                    data-icon="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($crypto_currency->short_name) . '.svg' ?>"
                                    value="<?= $crypto_currency->ratio ?>"><?= $crypto_currency->name ?>
                                (<?= $crypto_currency->short_name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                </select>
            </div>
            <div class="item-wrap">
                <span class="label-name"></span>
                <div class="flex">
                    <button type="submit" class="calculate-switch btn btn-primary">
                        <i class="fas fa-sync-alt"></i>
                    </button>
                </div>
            </div>
            <div class="item-wrap">
                <span class="label-name"><?= ctranslate('translation_convert_to') ?></span>
                <select class="convert_to_list calculate-price form-control"
                        name="convert-list"
                        data-default="<?= $convert_to_currency ?>"
                >
                    <optgroup label="Fiat Currencies">
                        <?php foreach ($currencies as $currency) { ?>
                            <option data-slug="<?= $currency->name ?>"
                                    data-full-name="<?= $currency->full_name ?>"
                                    data-icon="<?= Crypto_Calculator_URL . 'assets/images/currencies/' . strtolower($currency->name) . '.svg' ?>"
                                    value="<?= $currency->price ?>"><?= $currency->full_name ?> (<?= $currency->name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                    <optgroup label="Crypto Currencies">
                        <?php foreach ($crypto_currencies as $crypto_currency) { ?>
                            <option data-slug="<?= $crypto_currency->short_name ?>"
                                    data-full-name="<?= $crypto_currency->name ?>"
                                    data-icon="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($crypto_currency->short_name) . '.svg' ?>"
                                    value="<?= $crypto_currency->ratio ?>"><?= $crypto_currency->name ?>
                                (<?= $crypto_currency->short_name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                </select>
            </div>

        </div>
        <div class="footer">
            <div class="result-calculator">
                <span class="base"></span>
                <span class="equals">=</span>
                <span class="convert"></span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  jQuery(function ($) {
    let element = $('.floating-badge')
    let wrapper = $('.crypto-calculator-wrapper')

    setTimeout(function () {
      element.addClass('enter')
    }, 200)

    element.click(openElement)

    function openElement() {
      element.find('>img').hide()
      element.addClass('expand')
      wrapper.addClass('expand')
      element.find('.badge-calculator').addClass('enter')
      setTimeout(function () {
        element.addClass('auto-height')
        $('html').addClass('calculator-active')
        element.off('click', openElement)
        element.find('.header button').click(closeElement)

        element.find('.badge-calculator').show()
      }, 200)

      setTimeout(function () {
        element.find('.badge-calculator').css({ opacity: 1 })
      }, 300)
    }

    function closeElement() {
      element.removeClass('auto-height')
      element.find('.badge-calculator').removeClass('enter').hide().css({ opacity: 0 })
      element.find('>img').show()
      element.removeClass('expand')
      wrapper.removeClass('expand')

      $('html').removeClass('calculator-active')
      element.find('.header button').off('click', closeElement)
      setTimeout(function () {
        element.click(openElement)
      }, 500)
    }
  })
</script>
