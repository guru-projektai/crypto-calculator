<div class="crypto-calculator calc-wrapper <?= $type ?>">
    <div class="calc-container">
        <h4 class="title"><?= ctranslate('translation_title') ?></h4>

        <div class="form-group">
            <label for="amount"><?= ctranslate('translation_enter_amount') ?></label>
            <input type="number"
                   name="amount"
                   value="1"
                   class="form-control amount calculate-price" />
        </div>
        <div class="wrap">
            <div class="item-wrap">
                <span class="label-name"><?= ctranslate('translation_base_currency') ?></span>
                <select class="base_list calculate-price form-control"
                        name="base-currency"
                        data-default="<?= $base_currency ?>"
                >
                    <optgroup label="Fiat Currencies">
                        <?php foreach ($currencies as $currency) { ?>
                            <option data-slug="<?= $currency->name ?>"
                                    data-full-name="<?= $currency->full_name ?>"
                                    data-icon="<?= Crypto_Calculator_URL . 'assets/images/currencies/' . strtolower($currency->name) . '.svg' ?>"
                                    value="<?= $currency->price ?>"><?= $currency->full_name ?> (<?= $currency->name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                    <optgroup label="Crypto Currencies">
                        <?php foreach ($crypto_currencies as $crypto_currency) { ?>
                            <option data-slug="<?= $crypto_currency->short_name ?>"
                                    data-full-name="<?= $crypto_currency->name ?>"
                                    data-icon="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($crypto_currency->short_name) . '.svg' ?>"
                                    value="<?= $crypto_currency->ratio ?>"><?= $crypto_currency->name ?>
                                (<?= $crypto_currency->short_name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                </select>
            </div>
            <button type="submit" class="calculate-switch btn btn-primary">
                <i class="fas fa-sync-alt"></i>
            </button>
            <div class="item-wrap">
                <span class="label-name"><?= ctranslate('translation_convert_to') ?></span>
                <select class="convert_to_list calculate-price form-control"
                        name="convert-list"
                        data-default="<?= $convert_to_currency ?>"
                >
                    <optgroup label="Fiat Currencies">
                        <?php foreach ($currencies as $currency) { ?>
                            <option data-slug="<?= $currency->name ?>"
                                    data-full-name="<?= $currency->full_name ?>"
                                    data-icon="<?= Crypto_Calculator_URL . 'assets/images/currencies/' . strtolower($currency->name) . '.svg' ?>"
                                    value="<?= $currency->price ?>"><?= $currency->full_name ?> (<?= $currency->name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                    <optgroup label="Crypto Currencies">
                        <?php foreach ($crypto_currencies as $crypto_currency) { ?>
                            <option data-slug="<?= $crypto_currency->short_name ?>"
                                    data-full-name="<?= $crypto_currency->name ?>"
                                    data-icon="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($crypto_currency->short_name) . '.svg' ?>"
                                    value="<?= $crypto_currency->ratio ?>">
                                <?= $crypto_currency->name ?> (<?= $crypto_currency->short_name ?>)
                            </option>
                        <?php } ?>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="result-calculator">
            <span class="base"></span>
            <span class="equals">=</span>
            <span class="convert"></span>
        </div>
        <div class="share-conversion">
            <div class="copy-button" data-clipboard-text="">
                <i class="fas fa-copy"></i>
                <?= ctranslate('translation_copy_conversion') ?>
            </div>
        </div>
    </div>
</div>
