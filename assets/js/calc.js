window.theme = window.theme || {};

(function ($) {

    theme.config = {
      defaults: {
        base: 'BTC',
        currency: 'EUR'
      },
      selectors: {
        baseList: '.base_list',
        convertToList: '.convert_to_list',
        inputAmount: '.amount',
        calculationTrigger: '.calculate-price',
        results: '.result-calculator',
        switch: '.calculate-switch',
        saveConversion: '.save-conversion',
        shareConversion: '.share-conversion',
        copy: '.copy-button',
        copyInput: '.copy-input',
      },
      defaultValues: {
        baseList: null,
        convertToList: null
      },
      params: {
        base: null,
        convert: null,
        amount: null
      },
      isBadge: false,
    }

    theme.init = () => {
      theme.getParams()
      theme.initSelect2()
      theme.initInputs()
      theme.initBindings()
      theme.initClipboard()
      theme.setInputs()

      // Trigger event manually for initial load
      $(theme.config.selectors.calculationTrigger).change()
    }

    theme.initInputs = () => {
      if (theme.getParams()) {
        $(theme.config.selectors.inputAmount).val(theme.config.params.amount)
      }
    }

    theme.getParams = () => {
      const url = new URL(window.location.href)

      theme.config.params.base = url.searchParams.get('base')
      theme.config.params.convert = url.searchParams.get('convert')
      theme.config.params.amount = url.searchParams.get('amount')

      return Object.values(theme.config.params).every(function (val) {
        return val !== null && val !== ''
      })
    }

    theme.setParams = (params) => {
      theme.config.params = {...theme.config.params, ...params }
      // if (php.isPage !== 'true' || theme.config.isBadge) return false

      // if ('URLSearchParams' in window) {
      //   let searchParams = new URLSearchParams(window.location.search)
      //
      //   params.forEach(function (item) {
      //     searchParams.set(Object.keys(item)[0], Object.values(item)[0])
      //   })
      //
      //   let newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
      //   history.pushState(null, '', newRelativePathQuery);
      // }
    }

    theme.setInputFilter = (textbox, inputFilter) => {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.each((key, text) => {
          text.addEventListener(event, function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
          });
        })
      });
    }

    // Let filter through input text with regex
    theme.setInputs = () => {
      theme.setInputFilter($('.amount.calculate-price'), function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); // Float (accept both '.' and ',' as decimal separator)
      });
    }

    theme.initBindings = () => {
      $(theme.config.selectors.calculationTrigger).on('change keyup', function () {
        let parent = $(this).closest('.calc-wrapper')

        let baseList = parent.find(theme.config.selectors.baseList)
        let selectedConvertToOption = parent.find(theme.config.selectors.convertToList).find(':selected')
        let selectedBaseOption = parent.find(theme.config.selectors.baseList).find(':selected')
        let convertToList = parent.find(theme.config.selectors.convertToList)
        // let coinShortName = parent.find(theme.config.selectors.baseList).first().find(':selected').attr('data-slug')
        let inputAmount = parent.find(theme.config.selectors.inputAmount)

        let amount = $(inputAmount).val()
        if (amount === '') {
          amount = 1
        }

        let baseCurrency = baseList.val()

        theme.setParams({
            base: selectedBaseOption.data('slug'),
            convert: selectedConvertToOption.data('slug'),
            amount
        })

        let calculatePrice = theme.calculate(parent, amount, baseCurrency, convertToList)

        $(theme.config.selectors.shareConversion).click()

        amount = amount.toString().replace(',', '.')

        let baseString = amount +
            '<span class="full-name">' + selectedBaseOption.data('full-name') + '</span>' +
            '<span class="short-name-slug">' +
            '<span class="bracket">(</span>' + selectedBaseOption.data('slug') + '<span class="bracket">)</span>' +
            '</span>'

        let convertToString = theme.formatNumbersWithCommas(calculatePrice) +
            '<span class="full-name">' + selectedConvertToOption.data('full-name') + '</span>' +
            '<span class="short-name-slug">' +
            '<span class="bracket">(</span>' + selectedConvertToOption.data('slug') + '<span class="bracket">)</span>' +
            '</span>'

        parent.find(theme.config.selectors.results).find('.base').empty().append(
          theme.removeWhiteSpaceHTMLTags(baseString)
        )

        parent.find(theme.config.selectors.results).find('.convert').empty().append(
          theme.removeWhiteSpaceHTMLTags(convertToString)
        )
      })

      $(theme.config.selectors.switch).on('click', function () {
        const parent = $(this).closest('.calc-wrapper')

        let convertToList = parent.find(theme.config.selectors.convertToList)
        let baseList = parent.find(theme.config.selectors.baseList)

        let convertToListVal = convertToList.val()
        let baseListVal = baseList.val()

        convertToList.val(baseListVal).trigger('change.select2')
        baseList.val(convertToListVal).trigger('change.select2')

        parent.find(theme.config.selectors.calculationTrigger).change()
      })

      $(theme.config.selectors.shareConversion).on('click', function () {
        let parent = $(this).closest('.calc-wrapper')
        const params = Object.entries(theme.config.params)
        let newRelativePathQuery = window.location.href

        if ('URLSearchParams' in window) {
          let searchParams = new URLSearchParams(window.location.search)

          params.forEach(function (item) {
            searchParams.set(item[0], item[1])
          })

          newRelativePathQuery = window.location.origin + window.location.pathname + '?' + searchParams.toString();
        }

        let copyButton = parent.find(theme.config.selectors.copy)

        copyButton.attr('data-clipboard-text', newRelativePathQuery)
      })
    }

    theme.removeWhiteSpaceHTMLTags = (string) => {
      return string.replace(/\n/g, "")
        .replace(/[\t ]+\</g, "<")
        .replace(/\>[\t ]+\</g, "><")
        .replace(/\>[\t ]+$/g, ">")
    }

    theme.initClipboard = () => {
      let clipboard = new ClipboardJS(theme.config.selectors.copy)

      clipboard.on('success', function (e) {
        theme.setTooltip(e.trigger, 'Copied!')
        e.clearSelection()
      })

      $(theme.config.selectors.copy).tooltip({
        trigger: 'click',
        placement: 'bottom'
      })
    }

    theme.setTooltip = (el, message) => {
      $(el).attr('data-original-title', message)
        .tooltip('show')

      setTimeout(function() {
        $(el).tooltip('hide')
          .attr('data-original-title', "")
      }, 1000)
    }


    theme.calculate = (parent, amount, base, convertTo) => {
      amount = amount.toString().replace(',', '.')

      var base_currency_text = parent.find(theme.config.selectors.baseList).find(':selected').text().trim();
      var regExp = /\(([^)]+)\)/;
      var matches = regExp.exec(base_currency_text);
      var base_currency_code = matches != null ? (matches[1]) : false;

      let numbersAfterDecimal = base_currency_code == 'DOGE' ? 3 : 2;

      if (convertTo.find('option:selected').closest('optgroup').prop('label') === 'Crypto Currencies') {
        numbersAfterDecimal = 8;
      }

      return (amount * (parseFloat(convertTo.val()) / parseFloat(base))).toFixed(numbersAfterDecimal)
    }

    theme.initSelect2 = () => {
      $(theme.config.selectors.baseList).each(function(key, item) {
        let defaultItemElVal = theme.getParams() ? theme.config.params.base : $(item).data('default')
        let defaultItem = $(item).find('[data-slug="' + defaultItemElVal + '"]')

        $(item).select2({
          templateResult: theme.formatState,
          templateSelection: theme.formatState,
          escapeMarkup: (text) => {
            return text
          }
        }).val(defaultItem.val()).trigger('change.select2')
      })

      $(theme.config.selectors.convertToList).each(function(key, item) {
        let defaultItemElVal = theme.getParams() ? theme.config.params.convert : $(item).data('default')
        let defaultItem = $(item).find('[data-slug="' + defaultItemElVal.toUpperCase() + '"]')

        $(this).select2({
          templateResult: theme.formatState,
          templateSelection: theme.formatState,
          escapeMarkup: (text) => {
            return text
          }
        }).val(defaultItem.val()).trigger('change.select2')
      })
    }

    theme.formatState = (state) => {
      if ( ! state.id) return state.text

      const icon = $(state.element).attr('data-icon')

      let dataIcon = ''

      if (icon) {
        dataIcon = '<img class="calc-item-icon" src="' + icon + '" onerror="this.onerror=null;this.src=\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAQAAAAngNWGAAAAFElEQVR42mNkIBIwjiocVTiqEAQAJNYAFd868w4AAAAASUVORK5CYII=\'" />'
      }

      return $(`<div class="item-icon-wrapper">
                    <div class="wrap-left">
                      ${dataIcon}
                      <span class="full-name">${$(state.element).data('full-name')}</span>
                    </div>
                    <span class="short-name-slug">${$(state.element).data('slug')}</span>
              </div>`)
    }

    theme.formatNumbersWithCommas = (x) => {
      let parts = x.toString().split('.')
      parts[ 0 ] = parts[ 0 ].replace(/\B(?=(\d{3})+(?!\d))/g, ',')

      return parts.join('.')
    }

    $(document).ready(function () {
      theme.init()
    })

  }
)(jQuery)
