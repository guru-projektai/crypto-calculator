<?php

class Crypto_Calculator_Widget extends WP_Widget {

    const WIDGET_ID = 'crypto_calculator_widget';
    const WIDGET_NAME = 'Crypto calculator widget';

    /**
     * Crypto_Currency_Quotes_Widget constructor.
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => self::WIDGET_ID,
            'description' => 'A widget for crypto calculator',
        );

        parent::__construct(self::WIDGET_ID, self::WIDGET_NAME, $widget_ops);
    }

    /**
     * Frontend display of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance) {
        if (! get_plugin_options(Crypto_Calculator_Admin::ACTIVATED)) return;

        $show_currencies = [
            'USD', 'EUR', 'RUB', 'GBP', 'CHF', 'CAD', 'AUD', 'JPY', 'ZAR', 'CNY', 'SEK', 'SGD', 'HKD', 'NOK', 'TRY', 'INR', 'KES'
        ];
        $currencies = crypto_calc_get_currency_data($show_currencies);
        $crypto_currencies = crypto_calc_get_crypto_currency_data(100);

        $title = ctranslate('translation_title');
        $base_currency = CRYPTO_CALC_DEFAULT_BASE;
        $convert_to_currency = CRYPTO_CALC_DEFAULT_CONVERT_TO;
        $based_on_ip = true;

        extract( $args );

        echo $before_widget;

        if ( ! empty( $instance['base_currency'] ) ) {
            $base_currency = $instance['base_currency'];
        }

        if ( ! empty( $instance['convert_to_currency'] ) ) {
            $convert_to_currency = $instance['convert_to_currency'];
        }

        $based_on_ip = isset($instance['based_on_ip']) ? 'true' : 'false';


        if ($based_on_ip == 'true') {
            $country_info = apply_filters('ip_get_country_info', 2);

            if (count($country_info) == 1 ) {
                // Rebase key
                $country_info = array_values($country_info);
                $convert_to_currency = $country_info[0]->{'ISO4217-currency_alphabetic_code'};
            }
        }

        include Crypto_Calculator_PATH . 'templates/widget/display.php';

        echo $after_widget;
    }

    /**
     * Admin menu form logic
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance) {
        $title = esc_html__('Crypto Converter', 'text_domain');
        if (is_wpml_activate()) {
            $title = 'translation_title';
        }

        $title = ! empty( $instance['title'] ) ? $instance['title'] : $title;
        $base_currency = ! empty( $instance['base_currency'] ) ? $instance['base_currency'] : CRYPTO_CALC_DEFAULT_BASE;
        $convert_to_currency = ! empty( $instance['convert_to_currency'] ) ? $instance['convert_to_currency'] : CRYPTO_CALC_DEFAULT_CONVERT_TO;
        $based_on_ip = checked( $instance[ 'based_on_ip' ], 'on', false);

        include Crypto_Calculator_PATH . 'templates/widget/admin.php';
    }

    /**
     * Admin logic to updated widget parameters
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = [];
        $instance['title'] = (! empty( $new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['base_currency'] = (! empty( $new_instance['base_currency'])) ? strip_tags($new_instance['base_currency']) : '';
        $instance['convert_to_currency'] = (! empty( $new_instance['convert_to_currency'])) ? strip_tags($new_instance['convert_to_currency']) : '';
        $instance['based_on_ip'] = $new_instance['based_on_ip'];

        return $instance;
    }
}

function crypto_calculator_sidebar_badge() {
    register_sidebar(
        [
            'name' => __( 'Crypto calculator sidebar badge', 'landx' ),
            'id' => 'crypt_calculator_sidebar_badge',
            'description' => __( 'Crypto calculator badge', 'landx' ),
            'before_widget' => '<div class="crypto-calculator-wrapper">',
            'after_widget' => '</div>',
        ]
    );
}

add_action('widgets_init', 'crypto_calculator_sidebar_badge');

add_action('widgets_init', function () {
    register_widget( 'Crypto_Calculator_Widget' );
});