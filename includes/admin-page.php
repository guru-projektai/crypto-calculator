<?php

class Crypto_Calculator_Admin
{
    // Add required additional fields to pop up on plugin settings
    // In order to update default fields if they're not set
    const REQUIRED_OPTION_FIELDS = [
        self::API_KEY_FIELD_NAME,
        self::API_URL_FIELD_NAME,
        self::BASE_CURRENCY_FIELD_NAME,
        self::LANGUAGE,
        self::ACTIVATED,
    ];

    const REQUIRED_OPTION_DEFAULT = [
        self::API_KEY_FIELD_NAME => CRYPTO_CALC_CURRENCY_API_KEY,
        self::API_URL_FIELD_NAME => CRYPTO_CALC_CURRENCY_API_URL,
        self::BASE_CURRENCY_FIELD_NAME => CRYPTO_CALC_BASE_CURRENCY,
        self::LANGUAGE => CRYPTO_CALC_DEFAULT_LANGUAGE,
        self::ACTIVATED => 0,
    ];

    const API_KEY_FIELD_NAME = 'currency_api_key';
    const API_URL_FIELD_NAME = 'currency_api_url';
    const BASE_CURRENCY_FIELD_NAME = 'base_currency';
    const LANGUAGE = 'language';
    const ACTIVATED = 'activated';

    /* Translations */
    const T_ENTER_AMOUNT = 'translation_enter_amount';
    const T_TITLE = 'translation_title';
    const T_BASE_CURRENCY = 'translation_base_currency';
    const T_CONVERT_TO = 'translation_convert_to';
    const T_COPY_CONVERSION = 'translation_copy_conversion';

    /** @var array */
    const TRANSLATIONS = [
        self::T_TITLE,
        self::T_ENTER_AMOUNT,
        self::T_BASE_CURRENCY,
        self::T_CONVERT_TO,
        self::T_COPY_CONVERSION,
    ];

    const UPDATING = 'updating';

    const OPTIONS = 'crypto_calculator_options';
    const MENU_URL = 'crypto_calculator_settings';
    const PAGE_NAME = 'crypto-calculator-settings';
    const OPTION_GROUP = 'crypto_calculator_group';

    /** @var array */
    private $options;

    /** @var string */
    private $options_name;

    /**
     * Crypto_Calculator_Admin constructor.
     */
    public function __construct()
    {
        $this->options_name = self::OPTIONS;

        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_options_page(
            'Crypto Converter settings',
            'Crypto Converter settings',
            'manage_options',
            self::MENU_URL,
            array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        $context = 'Crypto Calculator';

        $this->options = get_option(self::OPTIONS);
        ?>
        <div class="wrap">
            <h1>Crypto Converter settings</h1>
            <?php settings_errors(); ?>
            <form method="post" action="options.php">
                <?php
                settings_fields(self::OPTION_GROUP);
                do_settings_sections(self::PAGE_NAME);
                submit_button(); ?>
            </form>

            <?php if (is_wpml_activate()) { ?>
                Click <a href="/wp/wp-admin/admin.php?page=wpml-string-translation%2Fmenu%2Fstring-translation.php&context=<?= $context ?>">HERE</a> for translations
            <?php } ?>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            self::OPTION_GROUP,
            self::OPTIONS,
            array( $this, 'sanitize' )
        );

        /* Basic settings */
        add_settings_section(
            'setting_section_one',
            'Crypto Converter settings',
            array($this, 'print_section_one_info'),
            self::PAGE_NAME
        );

        add_settings_field(
            self::ACTIVATED,
            'Activated',
            array($this, 'activated_callback'),
            self::PAGE_NAME,
            'setting_section_one'
        );

        add_settings_field(
            self::API_URL_FIELD_NAME,
            'Currency API URL',
            array($this, 'api_url_callback'),
            self::PAGE_NAME,
            'setting_section_one'
        );

        add_settings_field(
            self::API_KEY_FIELD_NAME,
            'Currency API Key',
            array($this, 'api_key_callback'),
            self::PAGE_NAME,
            'setting_section_one'
        );

        add_settings_field(
            self::BASE_CURRENCY_FIELD_NAME,
            'Base currency (short currency name)',
            array($this, 'base_currency_callback'),
            self::PAGE_NAME,
            'setting_section_one'
        );

        add_settings_field(
            self::BASE_CURRENCY_FIELD_NAME,
            'Base currency (short currency name)',
            array($this, 'base_currency_callback'),
            self::PAGE_NAME,
            'setting_section_one'
        );


        if (! is_wpml_activate()) {

            add_settings_field(
                self::LANGUAGE,
                'Select translation language',
                array($this, 'language_select_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );

            add_settings_field(
                self::T_TITLE,
                'Enter title (translation)',
                array($this, 'title_translation_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );

            add_settings_field(
                self::T_ENTER_AMOUNT,
                'Enter amount (translation)',
                array($this, 'enter_amount_translation_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );

            add_settings_field(
                self::T_BASE_CURRENCY,
                'Base currency (translation)',
                array($this, 'base_currency_translation_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );

            add_settings_field(
                self::T_CONVERT_TO,
                'Convert to (translation)',
                array($this, 'convert_to_translation_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );

            add_settings_field(
                self::T_COPY_CONVERSION,
                'Copy conversion URL (translation)',
                array($this, 'copy_conversion_translation_callback'),
                self::PAGE_NAME,
                'setting_section_one'
            );
        }
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input
     * @return array
     */
    public function sanitize($input)
    {
        $updated = false;

        $new_input = array();

        $old_values = get_option(self::OPTIONS);

        if (! is_wpml_activate()) {
            // Update default translations only if language is changed
            if ($old_values[self::LANGUAGE] !== $input[self::LANGUAGE]) {
                update_option(self::UPDATING, true);
                do_action('set_default_translations', false, $input[self::LANGUAGE]);
                $updated = true;
            }
        }

        if( isset($input[self::ACTIVATED]) )
            $new_input[self::ACTIVATED] = $input[self::ACTIVATED];

        if( isset($input[self::API_URL_FIELD_NAME]) )
            $new_input[self::API_URL_FIELD_NAME] = sanitize_text_field($input[self::API_URL_FIELD_NAME]);

        if( isset($input[self::API_KEY_FIELD_NAME]) )
            $new_input[self::API_KEY_FIELD_NAME] = sanitize_text_field($input[self::API_KEY_FIELD_NAME]);

        if( isset($input[self::BASE_CURRENCY_FIELD_NAME]) )
            $new_input[self::BASE_CURRENCY_FIELD_NAME] = sanitize_text_field($input[self::BASE_CURRENCY_FIELD_NAME]);

        if( isset($input[self::LANGUAGE]) )
            $new_input[self::LANGUAGE] = $input[self::LANGUAGE];

        if (! is_wpml_activate()) {
            /* Translations */
            if (isset($input[self::T_TITLE]))
                $new_input[self::T_TITLE] = $updated ? get_option(self::OPTIONS)[self::T_TITLE]
                    : sanitize_text_field($input[self::T_TITLE]);

            if (isset($input[self::T_ENTER_AMOUNT]))
                $new_input[self::T_ENTER_AMOUNT] = $updated ? get_option(self::OPTIONS)[self::T_ENTER_AMOUNT]
                    : sanitize_text_field($input[self::T_ENTER_AMOUNT]);

            if (isset($input[self::T_BASE_CURRENCY]))
                $new_input[self::T_BASE_CURRENCY] = $updated ? get_option(self::OPTIONS)[self::T_BASE_CURRENCY]
                    : sanitize_text_field($input[self::T_BASE_CURRENCY]);

            if (isset($input[self::T_CONVERT_TO]))
                $new_input[self::T_CONVERT_TO] = $updated ? get_option(self::OPTIONS)[self::T_CONVERT_TO]
                    : sanitize_text_field($input[self::T_CONVERT_TO]);

            if (isset($input[self::T_COPY_CONVERSION]))
                $new_input[self::T_COPY_CONVERSION] = $updated ? get_option(self::OPTIONS)[self::T_COPY_CONVERSION]
                    : sanitize_text_field($input[self::T_COPY_CONVERSION]);
        }

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_one_info()
    {
        print 'Enter your crypto calculator settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function activated_callback()
    {
        $field_name = self::ACTIVATED;
        ?>
        <input type="checkbox" name="<?= $this->options_name . '[' .$field_name . ']' ?>" <?php checked( $this->options[$field_name], 1 ); ?> value="1">

        <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_url_callback()
    {
        $field_name = self::API_URL_FIELD_NAME;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }


    /**
     * Get the settings option array and print one of its values
     */
    public function language_select_callback()
    {
        $field_name = self::LANGUAGE;
        ?>
            <select name="<?= $this->options_name ?>[<?= $field_name ?>]" id="<?= $field_name ?>">
                <?php $selected = (isset( get_option(self::OPTIONS)[$field_name] ) && get_option(self::OPTIONS)[$field_name] === 'lt') ? 'selected' : '' ; ?>
                <option value="lt" <?php echo $selected; ?>>LT</option>
                <?php $selected = (isset( get_option(self::OPTIONS)[$field_name] ) && get_option(self::OPTIONS)[$field_name] === 'en') ? 'selected' : '' ; ?>
                <option value="en" <?php echo $selected; ?>>EN</option>
            </select>
        <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function enter_amount_translation_callback()
    {
        $field_name = self::T_ENTER_AMOUNT;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function title_translation_callback()
    {
        $field_name = self::T_TITLE;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function base_currency_translation_callback()
    {
        $field_name = self::T_BASE_CURRENCY;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function convert_to_translation_callback()
    {
        $field_name = self::T_CONVERT_TO;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function copy_conversion_translation_callback()
    {
        $field_name = self::T_COPY_CONVERSION;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_key_callback()
    {
        $field_name = self::API_KEY_FIELD_NAME;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function base_currency_callback()
    {
        $field_name = self::BASE_CURRENCY_FIELD_NAME;

        printf(
            '<input type="text" id="'.$field_name.'" name="'.$this->options_name.'['.$field_name.']" value="%s" />',
            isset( $this->options[$field_name] ) ? esc_attr( $this->options[$field_name]) : ''
        );
    }
}
