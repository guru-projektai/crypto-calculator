<?php

/**
 * Gets currency data from api
 *
 * @return bool
 */
function crypto_calc_get_currencies_data() {
    $options = get_plugin_options();

    if (! $options) return false;

    $api_url = $options[Crypto_Calculator_Admin::API_URL_FIELD_NAME];

    $request = wp_remote_get("{$api_url}");

    if (is_wp_error($request)) return false;

    $body = wp_remote_retrieve_body($request);

    $response = json_decode($body);

    if (! $response) return false;

    $api_data = [];

    if (count((array)$response) > 0) {
        $api_data = crypto_calc_transform_data($response);
    }

    $currencies = pods(CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE);

    $currencies_data = $currencies->find([
        'distinct' => FALSE,
        'limit' => 0,
    ]);

    $data = json_decode(json_encode($currencies_data->data()), true);

    foreach ($api_data as $item) {
        $db_items = crypto_calc_get_item_id_by($data, $item['name'], 'name');

        $params = [
            'pod' => CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE,
        ];

        // If item id exists - update, else create new one
        if ($db_items) {
            $params['id'] = $db_items[0]['id'];
            unset($item['permalink']);
        }

        $params['data'] = $item;

        pods_api()->save_pod_item($params);
    }
}

function get_used_currencies() {
    $site_domain = strtolower($_SERVER['SERVER_NAME']);

    $type = 'smartbettingguide';

    if (strpos($site_domain, 'smartbettingguide') === false)
        $type = 'smartcasinoguide';

    switch ($type) {
        case 'smartbettingguide':
            $params = array(
                'distinct' => TRUE,
                'where' =>
                    'bookmaker_info_currencies.id IS NOT NULL AND ' .
                    't.bookmaker_info_enabled = 1',
                'limit' => 0,
            );

            $bookmaker_info = pods('bookmaker_info');
            $bookmaker_info->find($params);
            $bookmaker_info_data = $bookmaker_info->data();

            $currencies = array();

            if (!empty($bookmaker_info_data)) {
                foreach ($bookmaker_info_data as $entry) {
                    $bookmaker_info_id = $entry->id;

                    $bookmaker_info_new = pods('bookmaker_info', $bookmaker_info_id);

                    $bookmaker_info_crypto_deposits = $bookmaker_info_new->field('bookmaker_info_currencies');


                    foreach ($bookmaker_info_crypto_deposits as $method) {
                        $currencies[] = $method;
                    }
                }
            }

            return array_map('unserialize', array_unique(array_map('serialize', $currencies)));
            break;

        case 'smartcasinoguide':
            // $params = array(
            //     'distinct' => TRUE,
            //     'where' =>
            //         'casino_info_currencies.id IS NOT NULL',
            //     'limit' => 0
            // );

            // $casino_info = pods('casino_info');
            // $casino_info->find($params);
            // $casino_info_data = $casino_info->data();

            $currencies = array();

            if (!empty($casino_info_data)) {
                foreach ($casino_info_data as $entry) {
                    $casino_info_id = $entry->id;

                    $casino_info_new = pods('casino_info', $casino_info_id);

                    $casino_info_currencies_deposits = $casino_info_new->field('casino_info_currencies');


                    foreach ($casino_info_currencies_deposits as $method) {
                        $currencies[] = $method;
                    }
                }
            }

            return array_map('unserialize', array_unique(array_map('serialize', $currencies)));
            break;
    }


}

/**
 * Transforms data into PODS array
 *
 * @param $data
 * @return array
 */
function crypto_calc_transform_data($data) {
    $array = [];

    $top_currencies = [
        'USD' => -10,
        'EUR' => -9,
        'JPY' => -8,
        'GBP' => -7,
        'AUD' => -6,
        'CAD' => -5,
        'CHF' => -4,
        'CNY' => -3,
        'HKD' => -2,
    ];

    foreach ($data as $key => $item) {
        $array[] = [
            'name' => $item->code,
            'permalink' => $key,
            'price' => $item->rate,
            'full_name' => $item->name,
            'currency_sign' => get_currency_details($key)->symbol_native,
            'rank' => isset($top_currencies[$item->code]) ? $top_currencies[$item->code] : 0,
        ];
    }

    $array[] = [
        'name' => 'USD',
        'permalink' => 'usd',
        'price' => 1,
        'full_name' => 'US Dollar',
        'currency_sign' => '$',
        'rank' => $top_currencies['USD']
    ];

    return $array;
}

/**
 * Get item id by specific column
 *
 * @param $array
 * @param $value
 * @param string $search_by_column
 * @return array
 */
function crypto_calc_get_item_id_by($array, $value, $search_by_column = 'name') {
    if (! is_array($array)) return null;

    $item = array_filter($array, function ($item) use ($value, $search_by_column) {
        return $item[$search_by_column] === $value;
    });

    // Reset keys
    if ($item) return array_values($item);
}

/**
 * Gets data from pods database
 *
 * @param array $filter
 * @return array|bool
 */
function crypto_calc_get_currency_data($filter = []) {
    $currencies = pods(CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE);

    $currencies_data = $currencies->find([
        'distinct' => FALSE,
        'where' => 'price IS NOT NULL AND full_name IS NOT NULL AND rank IS NOT NULL',
        'limit' => 0,
        'orderby' => 'rank ASC',
    ]);

    $data = $currencies_data->data();

    if (count($filter) > 0) {
        $data = array_filter($data, function ($item) use ($filter) {
            return in_array($item->name, $filter);
        });
    }

    return array_values($data);
}

/**
 * Gets data from pods database
 *
 * @param int $limit
 * @return array|bool
 */
function crypto_calc_get_crypto_currency_data($limit = 0) {
    $site_domain = strtolower($_SERVER['SERVER_NAME']);

    $type = 'smartbettingguide';

    if (strpos($site_domain, 'smartbettingguide') === false)
        $type = 'smartcasinoguide';

    $crypto_currencies = pods(CCQ_PODS_DATA_TABLE);

    $crypto_currencies_data = $crypto_currencies->find([
        'distinct' => FALSE,
        'orderby' => 'rank ASC',
        'limit' => $limit,
        'where' => 't.rank IS NOT NULL'
    ]);

    $crypto_currencies_list = $crypto_currencies_data->data();

    // SBG - Add more Cryptos.
    if ($type == 'smartbettingguide') {
        $extra_crypto_currencies_data = $crypto_currencies->find([
            'distinct' => FALSE,
            'orderby' => 'rank ASC',
            'where' => 't.name = "Dash" OR t.name = "Dogecoin" OR t.name = "QTUM" OR t.name = "OMG Network"',
        ]);

        $extra_crypto_currencies = $extra_crypto_currencies_data->data();

        $crypto_currencies_list = array_merge($crypto_currencies_list, $extra_crypto_currencies);
    }

    return $crypto_currencies_list;
}

/**
 * Get full description of currency
 *
 * @param $code
 * @return mixed
 */
function get_currency_details($code) {
    $code = strtoupper($code);
    $names = json_decode(file_get_contents(Crypto_Calculator_PATH . 'imports/currencies.json'));

    return $names->{$code};
}

/**
 * Returns country info based on user ip
 *
 * @return array|mixed|object
 * @throws Exception
 */
function get_country_info_by_user_ip_location() {
    // Make sure IP2Location database is exist.
    if (!is_file(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'))) {
        return;
    }

    if (!class_exists('IP2Location\\Database')) {
        require_once IP2LOCATION_REDIRECTION_ROOT . 'class.IP2Location.php';
    }

    $ip = custom_get_user_ip_address();

    // Create IP2Location object.
    $db = new \IP2Location\Database(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'), \IP2Location\Database::FILE_IO);

    // Get geolocation by IP address.
    $ip_geo_location = $db->lookup($ip, \IP2Location\Database::ALL);

    $countries_details = json_decode(file_get_contents(Crypto_Calculator_PATH . 'imports/country_currency_mapping.json'));

    $item = array_filter($countries_details, function ($item) use ($ip_geo_location) {
        if ($item->{'ISO3166-1-Alpha-2'} == $ip_geo_location['countryCode']) {
            return $item->{'ISO4217-currency_alphabetic_code'};
        }
    });

    return $item;
}

/**
 * Checks
 *
 * @param $column
 * @param $array
 * @return bool
 */
function db_exists_column($column, $array) {
    return array_key_exists($column, $array);
}

/**
 * Helper
 * Checks if array keys exists
 *
 * @param array $keys
 * @param array $arr
 * @return bool
 */
function crypto_calc_array_keys_exists(array $keys, array $arr) {
    return ! array_diff_key(array_flip($keys), $arr);
}

/**
 * @param $language_code
 * @return array
 */
function get_default_translations($language_code) {
    $translations = (array)json_decode(file_get_contents(Crypto_Calculator_PATH . "languages/{$language_code}.json"), true);

    return $translations;
}

/**
 * Translates string from plugin settings options
 * If WPML exists, take translation from WPML string translation
 *
 * @param $string
 * @param string $default_language
 * @return string
 */
function ctranslate($string, $default_language = CRYPTO_CALC_DEFAULT_LANGUAGE) {
    $default_translations = get_default_translations($default_language);

    // Take translations from WPML if WPML exists
    if (is_wpml_activate()) {
        return apply_filters('wpml_translate_single_string', $default_translations[$string], 'Crypto Calculator', $string);
    }

    $options = get_plugin_options();
    $translation = $options[$string];

    if (! isset($options[$string])) $translation = '';

    return $translation;
}

/**
 * Checks if WPML plugin is activated
 *
 * @return false|int
 */
function is_wpml_activate() {
    return class_exists('SitePress');
}

/**
 * Retrieves plugin options
 *
 * @param null $variable
 * @param null $default
 * @return mixed|void
 */
function get_plugin_options($variable = null, $default = null) {
    $options = get_option(Crypto_Calculator_Admin::OPTIONS);

    if ($options && $variable) {
        $options = $options[$variable];

        if ($options == '' || ! $options) {
            return $default;
        }
    }

    return $options;
}
