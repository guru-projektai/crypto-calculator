# Crypto Calculator plugin

Crypto calculator plugin (calculator) Crypto -> Real currency, Crypto -> Crypto

## Dependencies
* Crypto quotes plugin (in order to get crypto currencies list and their prices)

## Installation

Activate plugin - you're good to go.

In order to change CRYPTO_CALC_CURRENCY_API_URL, go to plugin settings and change it there. Make sure the api response is the same, otherwise changes needed.


## Development
In order to continue developing this plugin:

```bash
npm install
nvm use 11.15.0
npm rebuild node-sass
```
In order to run gulp and watch styles for changes (this will open new browser with browsersync which will reload only styles without reloading page):
```bash
npm run gulp watch:sass
```
Uncomment certain lines in enqueue() method for development


To compile scripts files for LIVE:
```bash
npm run gulp scripts
```

