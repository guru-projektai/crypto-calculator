<?php
/**
 * Plugin Name: Crypto calculator
 * Description: Crypto calculator (badge and shortcode)
 * Version: 1.0.1
 * Author: Mindaugas
 *
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Crypto_Calculator_FILE', __FILE__ );
define( 'Crypto_Calculator_PATH', plugin_dir_path( Crypto_Calculator_FILE ) );
define( 'Crypto_Calculator_URL', plugin_dir_url( Crypto_Calculator_FILE ) );
define( 'CRYPTO_CALC_CURRENCY_API_URL' , "http://www.floatrates.com/widget/00000807/7be0d42f700c33ed97a55869a34b9c36/usd.json" );
define( 'CRYPTO_CALC_CURRENCY_API_KEY' , "587553e0-f6f7-4635-93b1-cfc28e8f226f" );
define( 'CRYPTO_CALC_BASE_CURRENCY' , "USD" );
define( 'CRYPTO_CALC_DEFAULT_BASE' , "BTC" );
define( 'CRYPTO_CALC_DEFAULT_CONVERT_TO' , "USD" );
define( 'CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE' , "currency" );
define( 'CRYPTO_CALC_SCHEDULE_HOOK_NAME', "crypto_calc_update_data" );
define( 'CRYPTO_CALC_DEFAULT_LANGUAGE', "en" );

register_deactivation_hook( Crypto_Calculator_FILE, array( 'Crypto_Calculator', 'crypto_calc_deactivate' ) );

final class Crypto_Calculator {

    // Must update this array in the future when
    // adding new columns together with json migration
    const PLUGIN_DB_COLUMNS = [
        'name',
        'permalink',
        'price',
        'full_name',
        'currency_sign',
        'rank',
    ];

    /**
     * Plugin instance.
     *
     * @var Crypto_Calculator
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Crypto_Calculator
     * @static
     */
    public static function get_instance() {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Constructor.
     *
     * @access private
     */
    private function __construct() {
        register_activation_hook(Crypto_Calculator_FILE, array($this , 'crypto_calc_activate'));

        $this->crypto_calc_includes();

        new Crypto_Calculator_Admin();

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'crypto_calc_add_plugin_page_settings_link'));

        add_action('activated_plugin', array($this, 'save_error'));

        add_action('wp_enqueue_scripts', array($this, 'enqueue'));

        // Create shortcode to use it anywhere
        add_shortcode('crypto-calculator', array($this, 'crypto_calc_shortcode'));

        add_action('init', array($this, 'delete_duplicates'));

        // Create action in order to use it later for schedule events
        add_action(CRYPTO_CALC_SCHEDULE_HOOK_NAME, array($this, 'currencies_data_autoupdater'));

        // Adds new interval for cron events
        add_filter('cron_schedules', array($this, 'add_cron_recurrence_interval'));

        // Creates filter to use anywhere to get country info by user ip
        add_filter('ip_get_country_info', array($this, 'get_country_info_filter'));

        add_action('admin_init', array($this, 'migrate'));

        add_action('set_default_translations', array($this, 'register_translations'), 10, 2);

        // Update newly added translations automatically without reactivating plugin
        if (! get_option(Crypto_Calculator_Admin::UPDATING) && ! is_wpml_activate()) {
            $this->update_translations();
        }

        // Setups schedule
        $this->setup_schedule();
    }

    public function delete_duplicates()
    {
        if (! empty(get_option('crypto-calculator-currencies-remove-duplicates-2'))) return;

        $currencies = pods(CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE);

        $currencies_data = $currencies->find([
            'distinct' => FALSE,
            'limit' => 0,
        ]);

        $data = json_decode(json_encode($currencies_data->data()), true);

        $used_currencies = get_used_currencies();

        foreach ($data as $item) {
            $used = crypto_calc_get_item_id_by($used_currencies, $item['id'], 'id');
            if ($used && count($used) > 0) continue;

            pods_api()->delete_pod_item([
                'pod' => CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE,
                'id' => $item['id']
            ]);
        }

        $this->currencies_data_autoupdater();

        add_option('crypto-calculator-currencies-remove-duplicates-2', true);
    }

    /**
     * Enqueue styles and js
     */
    public function enqueue() {
        wp_enqueue_style( 'crypto-calc-styles', plugins_url( '/assets/dist/styles.css', __FILE__));

        // For production (npm run gulp scripts) to compile scripts

//        wp_register_script('calc-js', plugins_url('/assets/dist/js/compiled.js', __FILE__), array('jquery'));
//
//        wp_localize_script('calc-js', 'php', array(
//            'isPage' => is_page() ? 'true' : 'false',
//        ));
//        wp_enqueue_script('calc-js');

        // For development
        wp_enqueue_script('select2-js', plugins_url('/assets/js/select2.js', __FILE__), array('jquery'));
        wp_enqueue_script('clipboard-js', plugins_url('/assets/js/clipboard.js', __FILE__), array('jquery'));
        wp_enqueue_style( 'tooltip-styles', plugins_url( '/assets/css/jquery.tooltip.css', __FILE__ ));
        wp_register_script('tooltip-js', plugins_url('/assets/js/jquery.tooltip.js', __FILE__), array('jquery'),'1.3',false);
        wp_register_script('calc-js', plugins_url('/assets/js/calc.js', __FILE__), array('jquery','tooltip-js'),'1.0', true);

        wp_localize_script('calc-js', 'php', array(
            'isPage' => is_page() ? 'true' : 'false',
        ));

        wp_enqueue_script('calc-js');
    }

    /**
     * Saves error to txt file (in case)
     */
    public function save_error() {
        file_put_contents(dirname(__file__). '/error_activation.txt', ob_get_contents());
    }

    /**
     * Get Country info call to function
     *
     * @throws Exception
     */
    public function get_country_info() {
        add_action('get_country_info_user_ip', array($this, 'get_country_info_hook'));
    }

    /**
     * @throws Exception
     */
    public function get_country_info_filter() {
        $info = $this->get_country_info_hook();

        return $info;
    }

    /**
     * @return array|mixed|object
     * @throws Exception
     */
    public function get_country_info_hook() {
        $country_info = get_country_info_by_user_ip_location();

        return $country_info;
    }

    /**
     * Add settings page
     *
     * @param $links
     * @return array
     */
    public function crypto_calc_add_plugin_page_settings_link($links) {
        $links[] = '<a href="' .
            admin_url( 'options-general.php?page=' . Crypto_Calculator_Admin::MENU_URL ) .
            '">' . __('Settings') . '</a>';
        return $links;
    }

    /**
     * Run when deactivate plugin.
     *
     * Deletes pods database
     * Deletes plugin options
     * Deletes scheduled hooks
     */
    public static function crypto_calc_deactivate() {
        wp_unschedule_hook(CRYPTO_CALC_SCHEDULE_HOOK_NAME);
    }

    /**
     * Run when activate plugin.
     *
     * Runs migration (database)
     * Imports default settings (API URL, API key, etc...)
     * Fetches from API first time to have data
     */
    public function crypto_calc_activate() {
        if (current_user_can('activate_plugins') && ! class_exists('Crypto_Currency_Quotes')) {
            deactivate_plugins(plugin_basename(__FILE__));
            $error_message = '<p style="font-family:-apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,Oxygen-Sans,Ubuntu,Cantarell,\'Helvetica Neue\',sans-serif;font-size: 13px;line-height: 1.5;color:#444;">' . esc_html__( 'This plugin requires ', 'landx' ) . '<a href="#">Crypto currency quotes</a>' . esc_html__( ' plugin to be active.', 'landx' ) . '</p>';
            die($error_message); // WPCS: XSS ok.
        }

        $this->migrate();
        $this->import_default_settings();
        $this->currencies_data_autoupdater();
    }

    /**
     * Plugin shortcode
     *
     * @param array $atts
     * @return false|string
     * @throws Exception
     */
    public function crypto_calc_shortcode($atts = []) {
        if (! get_plugin_options(Crypto_Calculator_Admin::ACTIVATED)) return;

        $show_currencies = [
            'USD', 'EUR', 'RUB', 'GBP', 'CHF', 'CAD', 'AUD', 'JPY', 'ZAR', 'CNY', 'SEK', 'SGD', 'HKD', 'NOK', 'TRY', 'INR', 'KES'
        ];
        $currencies = crypto_calc_get_currency_data($show_currencies);
        $crypto_currencies = crypto_calc_get_crypto_currency_data(100);

        $base_currency = CRYPTO_CALC_DEFAULT_BASE;
        $convert_to_currency = CRYPTO_CALC_DEFAULT_CONVERT_TO;
        $title = 'translation_title';
        $type = 'post';
        $based_on_ip = true;

        if (isset($atts['title'])) {
            $title = $atts['title'];
        }

        if (isset($atts['type'])) {
            $type = $atts['type'];
        }

        if (isset($atts['base_currency'])) {
            $base_currency = $atts['base_currency'];
        }

        if (isset($atts['convert_to'])) {
            $convert_to_currency = $atts['convert_to'];
        }

        if (isset($atts['based_on_ip'])) {
            $based_on_ip = $atts['based_on_ip'];
        } else {
            $based_on_ip = isset($based_on_ip) ? 'true' : 'false';
        }

        if ($based_on_ip === 'true') {
            $country_info = $this->get_country_info_hook();

            if (count($country_info) == 1 ) {
                // Rebase key
                $country_info = array_values($country_info);
                $convert_to_currency = $country_info[0]->{'ISO4217-currency_alphabetic_code'};
            }
        }

        ob_start();

        include Crypto_Calculator_PATH . 'templates/crypto-calculator-shortcode.php';

        return ob_get_clean();
    }

    /**
     * Register cron event
     */
    public function setup_schedule() {
        if (! wp_next_scheduled(CRYPTO_CALC_SCHEDULE_HOOK_NAME)) {
            wp_schedule_event(time(), 'every_10_mins',CRYPTO_CALC_SCHEDULE_HOOK_NAME);
        }
    }

    /**
     * @param $schedules
     * @return mixed
     */
    public function add_cron_recurrence_interval($schedules) {
        $schedules['every_10_mins'] = array(
            'interval' => 600,
            'display' => __( 'Every 10 minutes', 'textdomain' )
        );

        return $schedules;
    }

    /**
     * Loading plugin functions files
     */
    public function crypto_calc_includes() {
        require_once __DIR__ . '/includes/crypto_calc_functions.php';
        require_once __DIR__ . '/includes/admin-page.php';
        require_once __DIR__ . '/includes/crypto_calc_widget.php';
    }

    /**
     * Updates default translations from specific language
     *
     * @param bool $refresh_new
     * @param string $language_code
     */
    public function register_translations($refresh_new = false, $language_code = CRYPTO_CALC_DEFAULT_LANGUAGE) {
        $old_options = get_plugin_options();

        $translations = get_default_translations($language_code);

        $new_translations = [];

        foreach (Crypto_Calculator_Admin::TRANSLATIONS as $translation) {
            if (! isset($old_options[$translation]) || ! strlen($old_options[$translation])) {
                $new_translations[$translation] = $translations[$translation];
            }
        }

        $old_options = $old_options ? $old_options : [];

        $merged = array_merge($old_options, $new_translations);

        if (! $refresh_new) {
            update_option(Crypto_Calculator_Admin::UPDATING, false);
            $merged = array_merge($old_options, $translations);
        }

        update_option(Crypto_Calculator_Admin::OPTIONS, $merged);
    }

    /**
     * Migrates columns to database table
     */
    public function migrate() {
        $pod = pods_api()->load_pod(CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE);

        $pod_id = ! empty($pod) ? $pod['id'] : false;

        if ($pod_id) {
            if (! crypto_calc_array_keys_exists(self::PLUGIN_DB_COLUMNS, $pod['fields'])) {
                $this->migrate_columns($pod);
                $this->currencies_data_autoupdater();
            }
        }
    }

    /**
     * Migrate columns
     */
    private function migrate_columns($pod) {
        /* Price field */
        if (! db_exists_column('price', $pod['fields'])) {
            $params = array(
                'pod'     => CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE,
                'name'    => 'price',
                'label'   => 'Price',
                'type'    => 'number',
                'options' => array(
                    'number_format'   => '9999.99',
                    'number_decimals' => 6,
                )
            );

            pods_api()->save_field($params);
        }

        /* Full name field */
        if (! db_exists_column('full_name', $pod['fields'])) {
            $params = array(
                'pod'    => CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE,
                'name'   => 'full_name',
                'label'  => 'Full name',
                'type'   => 'text',
            );

            pods_api()->save_field($params);
        }

        /* Rank field */
        if (! db_exists_column('rank', $pod['fields'])) {
            $params = array(
                'pod'    => CRYPTO_CALC_PODS_CURRENCY_DATA_TABLE,
                'name'   => 'rank',
                'label'  => 'Rank',
                'type'   => 'number',
            );

            pods_api()->save_field($params);
        }
    }

    /**
     * Update translations without reactivating plugin
     */
    public function update_translations()
    {
        $language = get_plugin_options(Crypto_Calculator_Admin::LANGUAGE, 'lt');

        do_action('set_default_translations', true, $language);
    }

    /**
     * Update database after specific interval
     */
    public function currencies_data_autoupdater() {
        crypto_calc_get_currencies_data();
    }

    /**
     * Imports default settings
     */
    private function import_default_settings() {
        $options = get_plugin_options();

        if (! $options) {
            return add_option(Crypto_Calculator_Admin::OPTIONS, [
                Crypto_Calculator_Admin::API_KEY_FIELD_NAME => CRYPTO_CALC_CURRENCY_API_KEY,
                Crypto_Calculator_Admin::API_URL_FIELD_NAME => CRYPTO_CALC_CURRENCY_API_URL,
                Crypto_Calculator_Admin::BASE_CURRENCY_FIELD_NAME => CRYPTO_CALC_BASE_CURRENCY,
                Crypto_Calculator_Admin::LANGUAGE => CRYPTO_CALC_DEFAULT_LANGUAGE,
            ]);
        }

        foreach (Crypto_Calculator_Admin::REQUIRED_OPTION_FIELDS as $field) {
            if (! array_key_exists($field, $options)) {
                $options[$field] = Crypto_Calculator_Admin::REQUIRED_OPTION_DEFAULT[$field];
            }
        }

        update_option(Crypto_Calculator_Admin::OPTIONS, $options);
    }

}

function Crypto_Calculator() {
    return Crypto_Calculator::get_instance();
}

$GLOBALS['Crypto_Calculator'] = Crypto_Calculator();
